# 移动部需求

根据股票ID获取自选股中订阅该股票的所有用户ID。

# 脚本说明

__config.py__

* redis_us: 源redis地址及主key.  该哈希存储所有自选股用户的订阅信息，子key 为用户id， value 为 json 格式信息，记录分组及股票ID等。
* redis_su: 目的redis地址及key前缀， ke前缀 +每个股票ID为key， value 为集合，存储所有订阅该股票的用户ID。

__export_stock_users.py__

源db导入到目的db的脚本。

__stat_user.py__

统计目的db中获取所有股票用户数耗时，用户数top20的股票及用户数，查询top10 的股票所有用户分别耗时。

