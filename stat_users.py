import redis
import config
from comman import stat_time


rdcli = redis.StrictRedis(config.redis_su["host"],
                            config.redis_su["port"],
                            config.redis_su["db"])


@stat_time
def getStocksUserNum():
    key_pattern = config.redis_su["namespace"] + "*"
    return {k.decode():rdcli.scard(k) for k in rdcli.scan_iter(match=key_pattern)}
    

def getSortedListByValue(d):
    return sorted(zip(d.values(), d.keys()), reverse=True)


@stat_time
def getSetMembers(key):
    return [i.decode() for i in rdcli.sscan_iter(key)]

@stat_time
def getSetMembers2(key):
    return [i.decode() for i in rdcli.smembers(key)]


if __name__ == "__main__":
    sl = getSortedListByValue(getStocksUserNum())
    print('\nNo.\tUsers\tKey')
    for i in range(20):
        n, k = sl[i]
        print("{0}\t{1}\t{2}".format(i+1, n, k))

    print("\nstart to fetch top 10\n")

    for i in range(10):
        _, k = sl[i]
        print("[{}]".format(k))
        users = getSetMembers(k)
        # 逗号分隔的长字符串，字符串长度基本相当于字节数
        print("return string length: {}\n".format(len(",".join(users))))
        getSetMembers2(k)
