import redis, json
import config
from comman import stat_time


rdcli = redis.StrictRedis(config.redis_us["host"],
                            config.redis_us["port"],
                            config.redis_us["db"])

pl = rdcli.pipeline(transaction=False)

input_file = 'bf1y_users.txt'
main_key = 'hset:emoney:user:mystock'


@stat_time
def deleteUsers():
    with open(input_file, 'r') as f:
        while True:
            lines = f.readlines(20)
            if not lines:
                break
            for line in lines:
                line = line.strip('\n')
                pl.hdel(main_key, line)
            pl.execute()



if __name__ == "__main__":
    deleteUsers()