import redis, json
import config
from comman import stat_time


rdcli_us = redis.StrictRedis(config.redis_us["host"],
                            config.redis_us["port"],
                            config.redis_us["db"])

rdcli_su = redis.StrictRedis(config.redis_su["host"],
                            config.redis_su["port"],
                            config.redis_su["db"])

pl_su = rdcli_su.pipeline(transaction=False)

user_hkey_num = 0


def getUserStocksRecs(key):
    """从源自选股缓存中读取记录
    每个用户的自选股记录为json数组，每个记录代表一个group的下的所有自选股信息
    源缓存结构参看:
    hget hset:emoney:user:mystock 0
"[{\"pid\":\"710000000\",\"group\":\"default\",\"stocks\":\"1000825,600176,600317,600109,601989,600125,1002520,601899,601390,1000401,1000983\",\"uid\":\"0\",\"TimeSpan\":1528735173629}]"
    """
    global user_hkey_num
    for hkey, value in rdcli_us.hscan_iter(key):
        user_hkey_num += 1
        try:
            for rec in json.loads(value):
                yield rec
        # 存在少量utf-8编码错误的value，json无法解析，抛弃
        except UnicodeDecodeError as e:
            pass
        print(user_hkey_num)


def recToStockUsersZSet(rec, nmspc, score):
    """根据读取到的记录存到stock=>user结构的缓存中
    以股票ID为Key存储订阅该股票的所有用户，以"defaut"分组下的信息为准
    """
    if rec['group'] == 'default':
        uid = rec['uid']
        # 屏蔽非法uid 包括超长，非数字 id
        if len(uid) == 10 and uid.isdigit():
            stocks = rec['stocks'].split(',')
            for s in stocks:
                ss = s.strip()
                # 屏蔽非法股票id 包括负数，超大数，非10进制数字
                if 0 < len(ss) < 10 and ss.isdigit():
                    s_id = '{:07}'.format(int(ss))
                    key = nmspc + s_id
                    pl_su.zadd(key, score, uid)
            pl_su.execute()


@stat_time
def userStocks2StockUsers(us_key, su_namespace):
    for score, rec in enumerate(getUserStocksRecs(us_key)):
        recToStockUsersZSet(rec, su_namespace, score)


if __name__ == "__main__":
    userStocks2StockUsers(config.redis_us["key"], config.redis_su["namespace"])
    print("finished")