import redis
import config
from comman import stat_time


rdcli = redis.StrictRedis(config.redis_su["host"],
                            config.redis_su["port"],
                            config.redis_su["db"])


def getKeyPattern():
    return config.redis_su["namespace"] + "*"

@stat_time
def getStocksUserNum():
    return {k.decode():rdcli.zcard(k) for k in rdcli.scan_iter(match=getKeyPattern())}
    

def getSortedListByValue(d):
    return sorted(zip(d.values(), d.keys()), reverse=True)


@stat_time
def getZSetMembers(key):
    return [i.decode() for i, _ in rdcli.zscan_iter(key)]

def zrange(key, start, end):
    return [i.decode() for i in rdcli.zrange(key, start, end)]

@stat_time
def zrange0_999(key):
    return zrange(key, 0, 1000)

@stat_time
def zrange1000_1999(key):
    return zrange(key, 1000, 2000)

@stat_time
def zrangeAll(key):
    return zrange(key, 0, -1)

@stat_time
def findUserAllStocks(uid):
    stocks = []
    k_pattern = getKeyPattern()
    for k in rdcli.scan_iter(match=k_pattern):
        if rdcli.zrank(k, uid):
            stock = k.decode().split(":")[-1]
            stocks.append(stock)
    print(stocks)



if __name__ == "__main__":
    #findUserAllStocks('2014689602')
    print(zrangeAll('users_sub_stock:0000001'))