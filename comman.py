import time

def stat_time(fn):
    def _wrapper(*args, **kwargs):
        start = time.time()
        ret = fn(*args, **kwargs)
        end = time.time()
        print("{}() cost {:.3f} seconds".format(fn.__name__, end - start))
        return ret
    return _wrapper